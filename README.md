Bunny
=====

A simple HTML templating language for Crystal

It uses the same syntax as ERB in ruby, cannot execute code in the view (yet). 
All variables must be in the locals hash and the value of the key replaces the code in the view.

See the example project in ./example for more information.
